//
//  Request.swift
//  Rest
//
//  Created by Delphinus on 6/5/15.
//  Copyright (c) 2015 Delphinus. All rights reserved.
//

import Foundation
/**
Responsible for sending a request and receiving the response and associated data from the server, as well as managing its underlying `NSURLSessionTask`.
*/
open class Request {
    
    // MARK: - Properties
    
    let delegate: TaskDelegate
    
    /// The underlying task.
    open var task: URLSessionTask { return delegate.task }
    
    /// The session belonging to the underlying task.
    open let session: URLSession
    
    /// The request sent or to be sent to the server.
    open var request: Foundation.URLRequest { return task.originalRequest! }
    
    /// The response received from the server, if any.
    open var response: HTTPURLResponse? { return task.response as? HTTPURLResponse }
    
    /// The progress of the request lifecycle.
    open var progress: Progress { return delegate.progress }
    
    // MARK: - Lifecycle
    
    @available(iOS 8.0, *)
    init(session: URLSession, task: URLSessionTask) {
        self.session = session
        
        switch task {
        case is URLSessionUploadTask:
            self.delegate = UploadTaskDelegate(task: task)
        case is URLSessionDataTask:
            self.delegate = DataTaskDelegate(task: task)
        case is URLSessionDownloadTask:
            self.delegate = DownloadTaskDelegate(task: task)
        default:
            self.delegate = TaskDelegate(task: task)
        }
    }
    
    // MARK: - Authentication
    
    /**
    Associates an HTTP Basic credential with the request.
    
    - parameter user: The user.
    - parameter password: The password.
    
    - returns: The request.
    */
    open func authenticate(user: String, password: String) -> Self {
        let credential = URLCredential(user: user, password: password, persistence: .forSession)
        
        return authenticate(usingCredential: credential)
    }
    
    /**
    Associates a specified credential with the request.
    
    - parameter credential: The credential.
    
    - returns: The request.
    */
    open func authenticate(usingCredential credential: URLCredential) -> Self {
        delegate.credential = credential
        
        return self
    }
    
    // MARK: - Progress
    
    /**
    Sets a closure to be called periodically during the lifecycle of the request as data is written to or read from the server.
    
    - For uploads, the progress closure returns the bytes written, total bytes written, and total bytes expected to write.
    - For downloads and data tasks, the progress closure returns the bytes read, total bytes read, and total bytes expected to read.
    
    - parameter closure: The code to be executed periodically during the lifecycle of the request.
    
    - returns: The request.
    */
    open func progress(_ closure: ((Int64, Int64, Int64) -> Void)? = nil) -> Self {
        if let uploadDelegate = delegate as? UploadTaskDelegate {
            uploadDelegate.uploadProgress = closure
        } else if let dataDelegate = delegate as? DataTaskDelegate {
            dataDelegate.dataProgress = closure
        } else if let downloadDelegate = delegate as? DownloadTaskDelegate {
            downloadDelegate.downloadProgress = closure
        }
        
        return self
    }
    
    // MARK: - Response
    
    /**
    A closure used by response handlers that takes a request, response, and data and returns a serialized object and any error that occured in the process.
    */
    public typealias Serializer = (Foundation.URLRequest, HTTPURLResponse?, Data?) -> (AnyObject?, NSError?)
    
    /**
    Creates a response serializer that returns the associated data as-is.
    
    - returns: A data response serializer.
    */
    open class func responseDataSerializer() -> Serializer {
        return { request, response, data in
            return (data, nil)
        }
    }
    
    /**
    Adds a handler to be called once the request has finished.
    
    - parameter completionHandler: The code to be executed once the request has finished.
    
    - returns: The request.
    */
    open func response(_ completionHandler: (Foundation.URLRequest, HTTPURLResponse?, AnyObject?, NSError?) -> Void) -> Self {
        return response(serializer: Request.responseDataSerializer(), completionHandler: completionHandler)
    }
    
    /**
    Adds a handler to be called once the request has finished.
    
    - parameter queue: The queue on which the completion handler is dispatched.
    - parameter serializer: The closure responsible for serializing the request, response, and data.
    - parameter completionHandler: The code to be executed once the request has finished.
    
    - returns: The request.
    */
    open func response(_ queue: DispatchQueue? = nil, serializer: @escaping Serializer, completionHandler: @escaping (Foundation.URLRequest, HTTPURLResponse?, AnyObject?, NSError?) -> Void) -> Self {
        delegate.queue.addOperation {
            
            var x = completionHandler
            var y = serializer
            let (responseObject, serializationError): (AnyObject?, NSError?) = serializer(self.request, self.response, self.delegate.data)
            
            (queue ?? DispatchQueue.main).async {
                completionHandler(self.request, self.response, responseObject, self.delegate.error ?? serializationError)
            }
        }
        
        return self
    }
    
    // MARK: - State
    
    /**
    Suspends the request.
    */
    open func suspend() {
        task.suspend()
    }
    
    /**
    Resumes the request.
    */
    open func resume() {
        task.resume()
    }
    
    /**
    Cancels the request.
    */
    open func cancel() {
        if let downloadDelegate = delegate as? DownloadTaskDelegate,
            let downloadTask = downloadDelegate.downloadTask
        {
            downloadTask.cancel { data in
                downloadDelegate.resumeData = data
            }
        } else {
            task.cancel()
        }
    }
    
    // MARK: - TaskDelegate
    
    class TaskDelegate: NSObject, URLSessionTaskDelegate {
        let task: URLSessionTask
        let queue: OperationQueue
        let progress: Progress
        
        var data: Data? { return nil }
        var error: NSError?
        
        var credential: URLCredential?
        
        @available(iOS 8.0, *)
        init(task: URLSessionTask) {
            self.task = task
            self.progress = Progress(totalUnitCount: 0)
            self.queue = {
                let operationQueue = OperationQueue()
                operationQueue.maxConcurrentOperationCount = 1
                operationQueue.isSuspended = true
                
                if operationQueue.responds(to: #selector(getter: Operation.qualityOfService)) {
                    operationQueue.qualityOfService = QualityOfService.utility
                }
                
                return operationQueue
                }()
        }
        
        deinit {
            queue.cancelAllOperations()
            queue.isSuspended = true
        }
        
        // MARK: - NSURLSessionTaskDelegate
        
        // MARK: Override Closures
        
        var taskWillPerformHTTPRedirection: ((Foundation.URLSession, URLSessionTask, HTTPURLResponse, Foundation.URLRequest) -> Foundation.URLRequest?)?
        var taskDidReceiveChallenge: ((Foundation.URLSession, URLSessionTask, URLAuthenticationChallenge) -> (Foundation.URLSession.AuthChallengeDisposition, URLCredential?))?
        var taskNeedNewBodyStream: ((Foundation.URLSession, URLSessionTask) -> InputStream?)?
        var taskDidCompleteWithError: ((Foundation.URLSession, URLSessionTask, NSError?) -> Void)?
        
        // MARK: Delegate Methods
        
        func urlSession(_ session: URLSession, task: URLSessionTask, willPerformHTTPRedirection response: HTTPURLResponse, newRequest request: URLRequest, completionHandler: (@escaping (Foundation.URLRequest?) -> Void)) {
            var redirectRequest: Foundation.URLRequest? = request
            
            if taskWillPerformHTTPRedirection != nil {
                redirectRequest = taskWillPerformHTTPRedirection!(session, task, response, request)
            }
            
            completionHandler(redirectRequest)
        }
        
        func urlSession(_ session: URLSession, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge, completionHandler: (@escaping (Foundation.URLSession.AuthChallengeDisposition, URLCredential?) -> Void)) {
            var disposition: Foundation.URLSession.AuthChallengeDisposition = .performDefaultHandling
            var credential: URLCredential?
            
            if taskDidReceiveChallenge != nil {
                (disposition, credential) = taskDidReceiveChallenge!(session, task, challenge)
            } else {
                if challenge.previousFailureCount > 0 {
                    disposition = .cancelAuthenticationChallenge
                } else {
                    credential = self.credential ?? session.configuration.urlCredentialStorage?.defaultCredential(for: challenge.protectionSpace)
                    
                    if credential != nil {
                        disposition = .useCredential
                    }
                }
            }
            
            completionHandler(disposition, credential)
        }
        
        func urlSession(_ session: URLSession, task: URLSessionTask, needNewBodyStream completionHandler: (@escaping (InputStream?) -> Void)) {
            var bodyStream: InputStream?
            
            if taskNeedNewBodyStream != nil {
                bodyStream = taskNeedNewBodyStream!(session, task)
            }
            
            completionHandler(bodyStream)
        }
        
        func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
            if taskDidCompleteWithError != nil {
                taskDidCompleteWithError!(session, task, error as NSError?)
            } else {
                if error != nil {
                    self.error = error as NSError?
                }
                
                queue.isSuspended = false
            }
        }
    }
    
    // MARK: - DataTaskDelegate
    
    class DataTaskDelegate: TaskDelegate, URLSessionDataDelegate {
        var dataTask: URLSessionDataTask? { return task as? URLSessionDataTask }
        
        fileprivate var mutableData: NSMutableData
        override var data: Data? {
            return mutableData as Data
        }
        
        fileprivate var expectedContentLength: Int64?
        
        
        var dataProgress: ((_ bytesReceived: Int64, _ totalBytesReceived: Int64, _ totalBytesExpectedToReceive: Int64) -> Void)?
        
        @available(iOS 8.0, *)
        override init(task: URLSessionTask) {
            self.mutableData = NSMutableData()
            super.init(task: task)
        }
        
        // MARK: - NSURLSessionDataDelegate
        
        // MARK: Override Closures
        
        var dataTaskDidReceiveResponse: ((Foundation.URLSession, URLSessionDataTask, URLResponse) -> Foundation.URLSession.ResponseDisposition)?
        var dataTaskDidBecomeDownloadTask: ((Foundation.URLSession, URLSessionDataTask, URLSessionDownloadTask) -> Void)?
        var dataTaskDidReceiveData: ((Foundation.URLSession, URLSessionDataTask, Data) -> Void)?
        var dataTaskWillCacheResponse: ((Foundation.URLSession, URLSessionDataTask, CachedURLResponse) -> CachedURLResponse?)?
        
        // MARK: Delegate Methods
        
        func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: (@escaping (Foundation.URLSession.ResponseDisposition) -> Void)) {
            var disposition: Foundation.URLSession.ResponseDisposition = .allow
            
            expectedContentLength = response.expectedContentLength
            
            if dataTaskDidReceiveResponse != nil {
                disposition = dataTaskDidReceiveResponse!(session, dataTask, response)
            }
            
            completionHandler(disposition)
        }
        
        func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didBecome downloadTask: URLSessionDownloadTask) {
            dataTaskDidBecomeDownloadTask?(session, dataTask, downloadTask)
        }
        
        func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
            if dataTaskDidReceiveData != nil {
                dataTaskDidReceiveData!(session, dataTask, data)
            } else {
                mutableData.append(data)
                
                let totalBytesReceived = Int64(mutableData.length)
                let totalBytesExpectedToReceive = dataTask.response?.expectedContentLength ?? NSURLSessionTransferSizeUnknown
                
                progress.totalUnitCount = totalBytesExpectedToReceive
                progress.completedUnitCount = totalBytesReceived
                
                dataProgress?(Int64(data.count), totalBytesReceived, totalBytesExpectedToReceive)
            }
        }
        
        func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, willCacheResponse proposedResponse: CachedURLResponse, completionHandler: (@escaping (CachedURLResponse?) -> Void)) {
            var cachedResponse: CachedURLResponse? = proposedResponse
            
            if dataTaskWillCacheResponse != nil {
                cachedResponse = dataTaskWillCacheResponse!(session, dataTask, proposedResponse)
            }
            
            completionHandler(cachedResponse)
        }
    }
}

// MARK: - Printable

extension Request: CustomStringConvertible {
    /// The textual representation used when written to an output stream, which includes the HTTP method and URL, as well as the response status code if a response has been received.
    public var description: String {
        var components: [String] = []
        if request.httpMethod != nil {
            components.append(request.httpMethod!)
        }
        
        components.append(request.URL!.absoluteString!)
        
        if response != nil {
            components.append("(\(response!.statusCode))")
        }
        
        return components.joined(separator: " ")
    }
}

// MARK: - DebugPrintable

extension Request: CustomDebugStringConvertible {
    func cURLRepresentation() -> String {
        var components: [String] = ["$ curl -i"]
        
        let URL = request.url
        
        if request.httpMethod != nil && request.httpMethod != "GET" {
            components.append("-X \(request.httpMethod!)")
        }
        
        if let credentialStorage = self.session.configuration.urlCredentialStorage {
            let protectionSpace = URLProtectionSpace(host: URL!.host!, port: (URL! as NSURL).port?.intValue ?? 0, protocol: URL!.scheme, realm: URL!.host!, authenticationMethod: NSURLAuthenticationMethodHTTPBasic)
            if let credentials = credentialStorage.credentials(for: protectionSpace)?.values {
                for credential in credentials {
                    components.append("-u \(credential.user!):\(credential.password!)")
                }
            } else {
                if let credential = delegate.credential {
                    components.append("-u \(credential.user!):\(credential.password!)")
                }
            }
        }
        
        // Temporarily disabled on OS X due to build failure for CocoaPods
        // See https://github.com/CocoaPods/swift/issues/24
        #if !os(OSX)
            if let cookieStorage = session.configuration.httpCookieStorage,
                let cookies = cookieStorage.cookies(for: URL!), !cookies.isEmpty
            {
                let string = cookies.reduce(""){ $0 + "\($1.name)=\($1.value ?? String());" }
                components.append("-b \"\(string.substring(to: string.characters.index(before: string.endIndex)))\"")
            }
        #endif
        
        if request.allHTTPHeaderFields != nil {
            for (field, value) in request.allHTTPHeaderFields! {
                switch field {
                case "Cookie":
                    continue
                default:
                    components.append("-H \"\(field): \(value)\"")
                }
            }
        }
        
        if session.configuration.httpAdditionalHeaders != nil {
            for (field, value) in session.configuration.httpAdditionalHeaders! {
                switch field {
                case "Cookie":
                    continue
                default:
                    components.append("-H \"\(field): \(value)\"")
                }
            }
        }
        
        if let HTTPBody = request.httpBody,
            let escapedBody = NSString(data: HTTPBody, encoding: String.Encoding.utf8.rawValue)?.replacingOccurrences(of: "\"", with: "\\\"")
        {
            components.append("-d \"\(escapedBody)\"")
        }
        
        components.append("\"\(URL!.absoluteString)\"")
        
        return components.joined(separator: " \\\n\t")
    }
    
    /// The textual representation used when written to an output stream, in the form of a cURL command.
    public var debugDescription: String {
        return cURLRepresentation()
    }
}
