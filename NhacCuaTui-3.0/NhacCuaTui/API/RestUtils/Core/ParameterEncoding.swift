//
//  ParameterEncoding.swift
//  Rest
//
//  Created by Delphinus on 6/5/15.
//  Copyright (c) 2015 Delphinus. All rights reserved.
//

import Foundation
/**
HTTP method definitions.

See http://tools.ietf.org/html/rfc7231#section-4.3
*/
public enum Method: String {
    case OPTIONS = "OPTIONS"
    case GET = "GET"
    case HEAD = "HEAD"
    case POST = "POST"
    case PUT = "PUT"
    case PATCH = "PATCH"
    case DELETE = "DELETE"
    case TRACE = "TRACE"
    case CONNECT = "CONNECT"
}

// MARK: - ParameterEncoding

/**
Used to specify the way in which a set of parameters are applied to a URL request.
*/
public enum ParameterEncoding {
    /**
    A query string to be set as or appended to any existing URL query for `GET`, `HEAD`, and `DELETE` requests, or set as the body for requests with any other HTTP method. The `Content-Type` HTTP header field of an encoded request with HTTP body is set to `application/x-www-form-urlencoded`. Since there is no published specification for how to encode collection types, the convention of appending `[]` to the key for array values (`foo[]=1&foo[]=2`), and appending the key surrounded by square brackets for nested dictionary values (`foo[bar]=baz`).
    */
    case url
    
    /**
    Uses `NSJSONSerialization` to create a JSON representation of the parameters object, which is set as the body of the request. The `Content-Type` HTTP header field of an encoded request is set to `application/json`.
    */
    case json
    
    /**
    Uses `NSPropertyListSerialization` to create a plist representation of the parameters object, according to the associated format and write options values, which is set as the body of the request. The `Content-Type` HTTP header field of an encoded request is set to `application/x-plist`.
    */
    case propertyList(PropertyListSerialization.PropertyListFormat, PropertyListSerialization.WriteOptions)
    
    /**
    Uses the associated closure value to construct a new request given an existing request and parameters.
    */
    case custom((URLRequestConvertible, [String: AnyObject]?) -> (Foundation.URLRequest, NSError?))
    
    /**
    Creates a URL request by encoding parameters and applying them onto an existing request.
    
    - parameter URLRequest: The request to have parameters applied
    - parameter parameters: The parameters to apply
    
    - returns: A tuple containing the constructed request and the error that occurred during parameter encoding, if any.
    */
    public func encode(_ URLRequest: URLRequestConvertible, parameters: [String: AnyObject]?) -> (Foundation.URLRequest, NSError?) {
        if parameters == nil {
            return (URLRequest.URLRequest as URLRequest, nil)
        }
        
        var mutableURLRequest: NSMutableURLRequest! = (URLRequest.URLRequest as NSURLRequest).mutableCopy() as! NSMutableURLRequest
        var error: NSError? = nil
        
        switch self {
        case .url:
            func query(_ parameters: [String: AnyObject]) -> String {
                var components: [(String, String)] = []
                for key in Array(parameters.keys).sorted(by: <) {
                    let value: AnyObject! = parameters[key]
                    components += self.queryComponents(key, value)
                }
                
                return (components.map{"\($0)=\($1)"} as [String]).joined(separator: "&")
            }
            
            func encodesParametersInURL(_ method: Method) -> Bool {
                switch method {
                case .GET, .HEAD, .DELETE:
                    return true
                default:
                    return false
                }
            }
            
            let method = Method(rawValue: mutableURLRequest.httpMethod)
            if method != nil && encodesParametersInURL(method!) {
                if let URLComponents = URLComponents(url: mutableURLRequest.url!, resolvingAgainstBaseURL: false) {
                    URLComponents.percentEncodedQuery = (URLComponents.percentEncodedQuery != nil ? URLComponents.percentEncodedQuery! + "&" : "") + query(parameters!)
                    mutableURLRequest.url = URLComponents.url
                }
            } else {
                if mutableURLRequest.value(forHTTPHeaderField: "Content-Type") == nil {
                    mutableURLRequest.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                }
                mutableURLRequest.httpBody = query(parameters!).data(using: String.Encoding.utf8, allowLossyConversion: false)
            }
        case .json:
            let options = JSONSerialization.WritingOptions()
            do {
                let data = try JSONSerialization.data(withJSONObject: parameters!, options: options)
                mutableURLRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
                mutableURLRequest.httpBody = data
            } catch let error1 as NSError {
                error = error1
            }
        case .propertyList(let (format, options)):
            do {
                let data = try PropertyListSerialization.data(fromPropertyList: parameters!, format: format, options: options)
                mutableURLRequest.setValue("application/x-plist", forHTTPHeaderField: "Content-Type")
                mutableURLRequest.httpBody = data
            } catch let error1 as NSError {
                error = error1
            }
        case .custom(let closure):
            return closure(mutableURLRequest as! URLRequestConvertible, parameters)
        }
        
        return (mutableURLRequest as URLRequest, error)
    }
    
    func queryComponents(_ key: String, _ value: AnyObject) -> [(String, String)] {
        var components: [(String, String)] = []
        if let dictionary = value as? [String: AnyObject] {
            for (nestedKey, value) in dictionary {
                components += queryComponents("\(key)[\(nestedKey)]", value)
            }
        } else if let array = value as? [AnyObject] {
            for value in array {
                components += queryComponents("\(key)[]", value)
            }
        } else {
            components.append(contentsOf: [(escape(key), escape("\(value)"))])
        }
        
        return components
    }
    
    /**
    Returns a percent escaped string following RFC 3986 for query string formatting.
    
    RFC 3986 states that the following characters are "reserved" characters.
    
    - General Delimiters: ":", "#", "[", "]", "@", "?", "/"
    - Sub-Delimiters: "!", "$", "&", "'", "(", ")", "*", "+", ",", ";", "="
    
    Core Foundation interprets RFC 3986 in terms of legal and illegal characters.
    
    - Legal Numbers: "0123456789"
    - Legal Letters: "abcdefghijklmnopqrstuvwxyz", "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    - Legal Characters: "!", "$", "&", "'", "(", ")", "*", "+", ",", "-",
    ".", "/", ":", ";", "=", "?", "@", "_", "~", "\""
    - Illegal Characters: All characters not listed as Legal
    
    While the Core Foundation `CFURLCreateStringByAddingPercentEscapes` documentation states
    that it follows RFC 3986, the headers actually point out that it follows RFC 2396. This
    explains why it does not consider "[", "]" and "#" to be "legal" characters even though
    they are specified as "reserved" characters in RFC 3986. The following rdar has been filed
    to hopefully get the documentation updated.
    
    - https://openradar.appspot.com/radar?id=5058257274011648
    
    In RFC 3986 - Section 3.4, it states that the "?" and "/" characters should not be escaped to allow
    query strings to include a URL. Therefore, all "reserved" characters with the exception of "?" and "/"
    should be percent escaped in the query string.
    
    - parameter string: The string to be percent escaped.
    
    - returns: The percent escaped string.
    */
    func escape(_ string: String) -> String {
        let generalDelimiters = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimiters = "!$&'()*+,;="
        
        let legalURLCharactersToBeEscaped: CFString = generalDelimiters + subDelimiters
        
        return CFURLCreateStringByAddingPercentEscapes(nil, string as CFString!, nil, legalURLCharactersToBeEscaped, CFStringBuiltInEncodings.UTF8.rawValue) as String
    }
}
