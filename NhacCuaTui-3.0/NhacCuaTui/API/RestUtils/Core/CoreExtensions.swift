 //
//  CoreExtensions.swift
//  Rest
//
//  Created by Delphinus on 6/6/15.
//  Copyright (c) 2015 Delphinus. All rights reserved.
//

import Foundation


extension Request {


    /**
    Adds a handler to be called once the request has finished.
    
    :param: completionHandler A closure to be executed once the request has finished and the data has been mapped to a swift Object. The closure takes 2 arguments: the response object (of type Mappable) and any error produced while making the request
    
    :returns: The request.
    */
    public func responseObject<T: Mappable>(_ completionHandler: @escaping (T?, NSError?) -> Void) -> Self {
        return responseObject(nil) { (request, response, object, data, error) -> Void in
            completionHandler(object, error)
        }
    }
    
    /**
    Adds a handler to be called once the request has finished.
    
    :param: completionHandler A closure to be executed once the request has finished and the data has been mapped to a swift Object. The closure takes 5 arguments: the URL request, the URL response, the response object (of type Mappable), the raw response data, and any error produced making the request.
    
    :returns: The request.
    */
    public func responseObject<T: Mappable>(_ completionHandler: @escaping (Foundation.URLRequest, HTTPURLResponse?, T?, AnyObject?, NSError?) -> Void) -> Self {
        return responseObject(nil, completionHandler: completionHandler)
    }
    
    /**
    Adds a handler to be called once the request has finished.
    
    :param: queue The queue on which the completion handler is dispatched.
    :param: completionHandler A closure to be executed once the request has finished and the data has been mapped to a swift Object. The closure takes 5 arguments: the URL request, the URL response, the response object (of type Mappable), the raw response data, and any error produced making the request.
    
    :returns: The request.
    */
    public func responseObject<T: Mappable>(_ queue: DispatchQueue?, completionHandler: @escaping (Foundation.URLRequest, HTTPURLResponse?, T?, AnyObject?, NSError?) -> Void) -> Self {
        return response(queue, serializer: Request.JSONResponseSerializer(JSONSerialization.ReadingOptions.allowFragments))
            { (request, response, data, error) -> Void in
                
                DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
//                    let parsedObjectCheck = Mapper<RestEntity>().map(data)
//
//                    if parsedObjectCheck?.message == "Error"{
////                        if(parsedObjectCheck?.message != nil){UIToast.makeText((parsedObjectCheck?.message)!).show()}
////                        else if(parsedObjectCheck?.error != nil){
////                            UIToast.makeText((parsedObjectCheck?.error)!).show()}
////                        
//                        
//                    }else {
                        let parsedObject = Mapper<T>().map(data)
                        print("Json: " + String(describing: data))
                    
                        (queue ?? DispatchQueue.main).async {
                            completionHandler(self.request, self.response, parsedObject, data, error)
                        }
                   // }
                }

                
        }
    }
    
    // MARK: Array responses
    
    /**
    Adds a handler to be called once the request has finished.
    
    :param: completionHandler A closure to be executed once the request has finished and the data has been mapped to a swift Object. The closure takes 2 arguments: the response array (of type Mappable) and any error produced while making the request
    
    :returns: The request.
    */
    public func responseArray<T: Mappable>(_ completionHandler: @escaping ([T]?, NSError?) -> Void) -> Self {
        return responseArray(nil) { (request, response, object, data, error) -> Void in
            completionHandler(object, error)
        }
    }
    
    /**
    Adds a handler to be called once the request has finished.
    
    :param: completionHandler A closure to be executed once the request has finished and the data has been mapped to a swift Object. The closure takes 5 arguments: the URL request, the URL response, the response array (of type Mappable), the raw response data, and any error produced making the request.
    
    :returns: The request.
    */
    public func responseArray<T: Mappable>(_ completionHandler: @escaping (Foundation.URLRequest, HTTPURLResponse?, [T]?, AnyObject?, NSError?) -> Void) -> Self {
        return responseArray(nil, completionHandler: completionHandler)
    }
    
    /**
    Adds a handler to be called once the request has finished.
    
    :param: queue The queue on which the completion handler is dispatched.
    :param: completionHandler A closure to be executed once the request has finished and the data has been mapped to a swift Object. The closure takes 5 arguments: the URL request, the URL response, the response array (of type Mappable), the raw response data, and any error produced making the request.
    
    :returns: The request.
    */
    public func responseArray<T: Mappable>(_ queue: DispatchQueue?, completionHandler: @escaping (Foundation.URLRequest, HTTPURLResponse?, [T]?, AnyObject?, NSError?) -> Void) -> Self {
        
        return response(queue, serializer: Request.JSONResponseSerializer(JSONSerialization.ReadingOptions.allowFragments)) { (request, response, data, error) -> Void in
            
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
                
                let parsedObject = Mapper<T>().mapArray(data)
                
                (queue ?? DispatchQueue.main).async {
                    completionHandler(self.request, self.response, parsedObject, data, error)
                }
            }
        }
    }
    
}
