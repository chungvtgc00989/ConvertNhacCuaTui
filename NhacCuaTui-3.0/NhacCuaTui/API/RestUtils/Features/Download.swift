//
//  Download.swift
//  Rest
//
//  Created by Delphinus on 6/5/15.
//  Copyright (c) 2015 Delphinus. All rights reserved.
//

import Foundation
@available(iOS 8.0, *)
extension Manager {
    fileprivate enum Downloadable {
        case request(Foundation.URLRequest)
        case resumeData(Data)
    }
    
    fileprivate func download(_ downloadable: Downloadable, destination: @escaping Request.DownloadFileDestination) -> Request {
        var downloadTask: URLSessionDownloadTask!
        
        switch downloadable {
        case .request(let request):
            queue.sync {
                downloadTask = self.session.downloadTask(with: request)
            }
        case .resumeData(let resumeData):
            queue.sync {
                downloadTask = self.session.downloadTask(withResumeData: resumeData)
            }
        }
        
        let request = Request(session: session, task: downloadTask)
        if let downloadDelegate = request.delegate as? Request.DownloadTaskDelegate {
            downloadDelegate.downloadTaskDidFinishDownloadingToURL = { session, downloadTask, URL in
                return destination(URL, downloadTask.response as! HTTPURLResponse)
            }
        }
        delegate[request.delegate.task] = request.delegate
        
        if startRequestsImmediately {
            request.resume()
        }
        
        return request
    }
    
    // MARK: Request
    
    /**
     Creates a download request using the shared manager instance for the specified method and URL string.
     
     - parameter method: The HTTP method.
     - parameter URLString: The URL string.
     - parameter destination: The closure used to determine the destination of the downloaded file.
     
     - returns: The created download request.
     */
    public func download(_ method: Method, _ URLString: URLStringConvertible, destination: Request.DownloadFileDestination) -> Request {
        //        return download(URLRequest(method, URLString: URLString), destination: destination)
        return download(method, URLString, destination: destination)
        
    }
    
    /**
     Creates a request for downloading from the specified URL request.
     
     If `startRequestsImmediately` is `true`, the request will have `resume()` called before being returned.
     
     - parameter URLRequest: The URL request
     - parameter destination: The closure used to determine the destination of the downloaded file.
     
     - returns: The created download request.
     */
    public func download(_ URLRequest: URLRequestConvertible, destination: Request.DownloadFileDestination) -> Request {
        //        return download(.request(URLRequest.URLRequest), destination: destination)
        return download(URLRequest, destination: destination)
    }
    
    // MARK: Resume Data
    
    /**
     Creates a request for downloading from the resume data produced from a previous request cancellation.
     
     If `startRequestsImmediately` is `true`, the request will have `resume()` called before being returned.
     
     - parameter resumeData: The resume data. This is an opaque data blob produced by `NSURLSessionDownloadTask` when a task is cancelled. See `NSURLSession -downloadTaskWithResumeData:` for additional information.
     - parameter destination: The closure used to determine the destination of the downloaded file.
     
     - returns: The created download request.
     */
    public func download(_ resumeData: Data, destination: Request.DownloadFileDestination) -> Request {
//        return download(.resumeData(resumeData), destination: destination)
        return download(resumeData, destination: destination)
    }
}

// MARK: -

extension Request {
    /**
     A closure executed once a request has successfully completed in order to determine where to move the temporary file written to during the download process. The closure takes two arguments: the temporary file URL and the URL response, and returns a single argument: the file URL where the temporary file should be moved.
     */
    public typealias DownloadFileDestination = (URL, HTTPURLResponse) -> URL
    
    /**
     Creates a download file destination closure which uses the default file manager to move the temporary file to a file URL in the first available directory with the specified search path directory and search path domain mask.
     
     - parameter directory: The search path directory. `.DocumentDirectory` by default.
     - parameter domain: The search path domain mask. `.UserDomainMask` by default.
     
     - returns: A download file destination closure.
     */
    public class func suggestedDownloadDestination(_ directory: FileManager.SearchPathDirectory = .documentDirectory, domain: FileManager.SearchPathDomainMask = .userDomainMask) -> DownloadFileDestination {
        
        return { temporaryURL, response -> URL in
//            if let directoryURL = FileManager.default.urls(for: directory, in: domain)[0] as? URL {
//                return directoryURL.URLByAppendingPathComponent(response.suggestedFilename!)!
//            }
            if let directoryURLs = FileManager.default.urls(for: directory, in: domain)[0] as? URL {
                
                return directoryURLs.absoluteURL.appendingPathComponent(response.suggestedFilename!)
            }
            return temporaryURL
        }
    }
    
    // MARK: - DownloadTaskDelegate
    
    class DownloadTaskDelegate: TaskDelegate, URLSessionDownloadDelegate {
        var downloadTask: URLSessionDownloadTask? { return task as? URLSessionDownloadTask }
        var downloadProgress: ((Int64, Int64, Int64) -> Void)?
        var resumeData: Data?
        override var data: Data? { return resumeData }
        
        // MARK: - NSURLSessionDownloadDelegate
        
        // MARK: Override Closures
        
        var downloadTaskDidFinishDownloadingToURL: ((Foundation.URLSession, URLSessionDownloadTask, URL) -> URL)?
        var downloadTaskDidWriteData: ((Foundation.URLSession, URLSessionDownloadTask, Int64, Int64, Int64) -> Void)?
        var downloadTaskDidResumeAtOffset: ((Foundation.URLSession, URLSessionDownloadTask, Int64, Int64) -> Void)?
        
        // MARK: Delegate Methods
        
        func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
            if downloadTaskDidFinishDownloadingToURL != nil {
                let destination = downloadTaskDidFinishDownloadingToURL!(session, downloadTask, location)
                var fileManagerError: NSError?
                
                do {
                    try FileManager.default.moveItem(at: location, to: destination)
                } catch let error as NSError {
                    fileManagerError = error
                }
                
                if fileManagerError != nil {
                    error = fileManagerError
                }
            }
        }
        
        func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
            if downloadTaskDidWriteData != nil {
                downloadTaskDidWriteData!(session, downloadTask, bytesWritten, totalBytesWritten, totalBytesExpectedToWrite)
            } else {
                progress.totalUnitCount = totalBytesExpectedToWrite
                progress.completedUnitCount = totalBytesWritten
                
                downloadProgress?(bytesWritten, totalBytesWritten, totalBytesExpectedToWrite)
            }
        }
        
        func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didResumeAtOffset fileOffset: Int64, expectedTotalBytes: Int64) {
            if downloadTaskDidResumeAtOffset != nil {
                downloadTaskDidResumeAtOffset!(session, downloadTask, fileOffset, expectedTotalBytes)
            } else {
                progress.totalUnitCount = expectedTotalBytes
                progress.completedUnitCount = fileOffset
            }
        }
    }
}
